#!/usr/bin/env python
# -*- coding: utf-8 -*

# from tkinter import *
import os
import time
import sys
from tkinter import messagebox, filedialog
from tkinter import *
import xlrd 
import xlwt
import collections
import operator
from operator import itemgetter
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
from resizeimage import resizeimage
from tqdm import tqdm
from tkinter.ttk import Progressbar
from tkinter import ttk


# args
# print(f"Arguments count: {len(sys.argv)}")

print(f"file: {sys.argv[1]}")

# /home/bruno/Github/AnaliseDistribuicaoGUI/SetembroTeste.xls

# for i, arg in enumerate(sys.argv):
#     print(f"Argument {i:>6}: {arg}")

# # GUI para escolha do ficheiro
# # guarda em root.filename o nome do ficheiro pretendido analisar
# root = Tk()
# # comando para esconder janela branca que aparece
# root.withdraw()
# root.filename = filedialog.askopenfilename(initialdir = "/home/bruno/Github/TFDanalytics",title = "Select file",filetypes = (("xls files","*.xls"),("all files","*.*")))
# # se e clicado o 'Cancel'
# if root.filename:
# 	try:
# 		print("Nome do ficheiro: {0}".format(root.filename))
# 	except:
# 		messagebox.showinfo("AVISO!!","Nome do Ficheiro nao valido")
# 		sys.exit()
# else:
# 	messagebox.showinfo("AVISO!!","Nenhum ficheiro selecionado")
# 	sys.exit()


filename=sys.argv[1]

# ------------------------
# Progress Bar
# ------------------------
# terminal
pbar = tqdm(total=100)

# inicio da leitura do ficheiro
book = xlrd.open_workbook(filename)
name=format(book.sheet_names())
# print(name)
sh=book.sheet_by_index(0)
# print("Numero de linhas: {0}".format(sh.nrows))
# print("Numero de colunas: {0}".format(sh.ncols))

# update terminal progress bar
pbar.update(5)

# Colunas a usar
# 'Viatura'
for i in range(sh.ncols):
	valor=sh.cell_value(rowx=0,colx=i)
	if valor == "Viatura":
		# print("Coluna onde esta 'Viatura': {0}".format(i))
		ColunaViatura=i
		break
	elif i == sh.ncols-1:
		# print("Erro!! Coluna 'Teste' nao encontrada")
		messagebox.showerror("Erro!!","Coluna 'Viatura' nao encontrada")
		sys.exit()

# 'TipoGuia'
for i in range(sh.ncols):
	valor=sh.cell_value(rowx=0,colx=i)
	if valor == "TipoGuia":
		ColunaTipoGuia=i
		break
	elif i == sh.ncols-1:
		# print("Erro!! Coluna 'Teste' nao encontrada")
		messagebox.showerror("Erro!!","Coluna 'TipoGuia' nao encontrada")
		sys.exit()
# 'ValorFatura'
for i in range(sh.ncols):
	valor=sh.cell_value(rowx=0,colx=i)
	if valor == "ValorFatura":
		ColunaValorFatura=i
		break
	elif i == sh.ncols-1:
		# print("Erro!! Coluna 'Teste' nao encontrada")
		messagebox.showerror("Erro!!","Coluna 'ValorFatura' nao encontrada")
		sys.exit()
# 'DataGuia'
for i in range(sh.ncols):
	valor=sh.cell_value(rowx=0,colx=i)
	if valor == "DataGuia":
		ColunaDataGuia=i
		break
	elif i == sh.ncols-1:
		# print("Erro!! Coluna 'Teste' nao encontrada")
		messagebox.showerror("Erro!!","Coluna 'DataGuia' nao encontrada")
		sys.exit()
# 'TotalKg'
for i in range(sh.ncols):
	valor=sh.cell_value(rowx=0,colx=i)
	if valor == "TotalKg":
		ColunaTotalKg=i
		break
	elif i == sh.ncols-1:
		# print("Erro!! Coluna 'Teste' nao encontrada")
		messagebox.showerror("Erro!!","Coluna 'TotalKg' nao encontrada")
		sys.exit()
# 'CodEstadoGuia'
for i in range(sh.ncols):
	valor=sh.cell_value(rowx=0,colx=i)
	if valor == "CodEstadoGuia":
		ColunaCodEstadoGuia=i
		break
	elif i == sh.ncols-1:
		# print("Erro!! Coluna 'Teste' nao encontrada")
		messagebox.showerror("Erro!!","Coluna 'CodEstadoGuia' nao encontrada")
		sys.exit()
# 'CalculoValor'
for i in range(sh.ncols):
	valor=sh.cell_value(rowx=0,colx=i)
	if valor == "CalculoValor":
		ColunaCalculoValor=i
		break
	elif i == sh.ncols-1:
		# print("Erro!! Coluna 'Teste' nao encontrada")
		messagebox.showerror("Erro!!","Coluna 'CalculoValor' nao encontrada")
		sys.exit()
# 'Cliente'
for i in range(sh.ncols):
	valor=sh.cell_value(rowx=0,colx=i)
	if valor == "Cliente":
		ColunaCliente=i
		break
	elif i == sh.ncols-1:
		# print("Erro!! Coluna 'Teste' nao encontrada")
		messagebox.showerror("Erro!!","Coluna 'Cliente' nao encontrada")
		sys.exit()

# array viaturas
viaturas=[]

#adicionar carros
for n in range(sh.nrows):
	nomeViatura=format(sh.cell_value(rowx=n, colx=ColunaViatura))
	v1={"viatura":nomeViatura,"entregas":0, "recolhas":0, "devolucoes":0, "prefatura":0, "faturado":0, "numdias":0, "pfpdt":0, "fpdt":0, "totalkg":0, "kgpdt":0, "valorkg":0, "pvalorkg":0, "minfd":0}
	if v1 in viaturas:
		i=0
	else:
		viaturas.append({"viatura":nomeViatura, "entregas":0, "recolhas":0, "devolucoes":0, "prefatura":0, "faturado":0, "numdias":0, "pfpdt":0, "fpdt":0, "totalkg":0,"kgpdt":0, "valorkg":0, "pvalorkg":0, "minfd":0})

# remover primeiro elemento pois e 'viatura'
viaturas.pop(0)

#remover viatura em branco, equivale aos cancelados
for i in range(len(viaturas)):
	if viaturas[i]["viatura"]=="":
		print("Endereco no array de viatura nula: {0}".format(i))
		viaturas.pop(i)
		break

# --------------------------------
# Update valores minimos viaturas
# --------------------------------
for n in range(len(viaturas)):
	if viaturas[n]["viatura"]=="01-RG-95":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="02-GV-58":
		viaturas[n]["minfd"]=150
	elif viaturas[n]["viatura"]=="03-RQ-89":
		viaturas[n]["minfd"]=150
	elif viaturas[n]["viatura"]=="03-SQ-68":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="06-RE-80":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="06-RE-81":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="12-91-VD":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="17-PR-84":
		viaturas[n]["minfd"]=280
	elif viaturas[n]["viatura"]=="17-PR-85":
		viaturas[n]["minfd"]=280
	elif viaturas[n]["viatura"]=="27-SS-97":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="35-AV-81":
		viaturas[n]["minfd"]=350
	elif viaturas[n]["viatura"]=="42-GI-96":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="46-FV-92":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="46-LR-83":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="47-VM-00":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="50-RC-97":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="55-VM-68":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="60-TV-81":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="61-MN-31":
		viaturas[n]["minfd"]=150
	elif viaturas[n]["viatura"]=="62-IV-22":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="62-OD-17":
		viaturas[n]["minfd"]=350
	elif viaturas[n]["viatura"]=="68-BI-72":
		viaturas[n]["minfd"]=150
	elif viaturas[n]["viatura"]=="75-SH-80":
		viaturas[n]["minfd"]=250
	elif viaturas[n]["viatura"]=="78-RE-60":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="84-EQ-82":
		viaturas[n]["minfd"]=350
	elif viaturas[n]["viatura"]=="94-DI-49":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="95-47-QL":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="95-52-TV":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="95-EQ-93":
		viaturas[n]["minfd"]=350
	elif viaturas[n]["viatura"]=="97-JS-48":
		viaturas[n]["minfd"]=350
	elif viaturas[n]["viatura"]=="98-SI-27":
		viaturas[n]["minfd"]=300
	elif viaturas[n]["viatura"]=="99-36-SA":
		viaturas[n]["minfd"]=200
	elif viaturas[n]["viatura"]=="99-SU-57":
		viaturas[n]["minfd"]=300

# estados da carga
estadocancelado=6

# percorrer xls a procura de ENTREGAS feitas por cada matricula
# format(sh.cell_value(rowx=n, colx=ColunaTipoGuia))
for n in range(sh.nrows):
	for i in range(len(viaturas)):
		nomeViatura=format(sh.cell_value(rowx=n, colx=ColunaViatura))
		tipoGuia=format(sh.cell_value(rowx=n,colx=ColunaTipoGuia))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if viaturas[i]["viatura"]==nomeViatura and tipoGuia=="DIST" and codestadoguia!=estadocancelado:
			viaturas[i]["entregas"]=viaturas[i]["entregas"]+1

# percorrer xls a procura de RECOLHAS feitas por cada matricula
# format(sh.cell_value(rowx=n, colx=ColunaTipoGuia))
for n in range(sh.nrows):
	for i in range(len(viaturas)):
		nomeViatura=format(sh.cell_value(rowx=n, colx=ColunaViatura))
		tipoGuia=format(sh.cell_value(rowx=n,colx=ColunaTipoGuia))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if viaturas[i]["viatura"]==nomeViatura and tipoGuia=="REC" and codestadoguia!=estadocancelado:
			viaturas[i]["recolhas"]=viaturas[i]["recolhas"]+1

#-------------------------------------------
#-------------UPDATE FATURACAO--------------
#-------------------------------------------

# percorrer xls a procura de DEVOLUCOES feitas por cada matricula
# format(sh.cell_value(rowx=n, colx=ColunaTipoGuia))
for n in range(sh.nrows):
	for i in range(len(viaturas)):
		nomeViatura=format(sh.cell_value(rowx=n, colx=ColunaViatura))
		tipoGuia=format(sh.cell_value(rowx=n,colx=ColunaTipoGuia))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if viaturas[i]["viatura"]==nomeViatura and tipoGuia=="DEV" and codestadoguia!=estadocancelado:
			viaturas[i]["devolucoes"]=viaturas[i]["devolucoes"]+1

# percorrer xls a procura do valor facturado por cada matricula
# format(sh.cell_value(rowx=n, colx=ColunaTipoGuia))
for n in range(sh.nrows):
	for i in range(len(viaturas)):
		nomeViatura=format(sh.cell_value(rowx=n, colx=ColunaViatura))
		valor=format(sh.cell_value(rowx=n,colx=ColunaValorFatura))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if valor== "ValorFatura":
			valor=0 
		if valor== "":
			valor=0
		valor=float(valor)
		if viaturas[i]["viatura"]==nomeViatura and codestadoguia!=estadocancelado:
			viaturas[i]["faturado"]=viaturas[i]["faturado"]+ valor
			
# percorrer xls a procura do valor Previsto de faturacao por cada matricula
for n in range(sh.nrows):
	for i in range(len(viaturas)):
		nomeViatura=format(sh.cell_value(rowx=n, colx=ColunaViatura))
		valor=format(sh.cell_value(rowx=n,colx=ColunaCalculoValor))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if valor== "CalculoValor":
			valor=0 
		if valor== "":
			valor=0
		valor=float(valor)
		if viaturas[i]["viatura"]==nomeViatura and codestadoguia!=estadocancelado:
			viaturas[i]["prefatura"]=viaturas[i]["prefatura"]+ valor
			

#-------------------------------------------
#------------UPDATE KG----------------------
#-------------------------------------------
# percorrer xls a procura do valor de KG de cada carga por cada matricula
for n in range(sh.nrows):
	for i in range(len(viaturas)):
		nomeViatura=format(sh.cell_value(rowx=n, colx=ColunaViatura))
		valor=format(sh.cell_value(rowx=n,colx=ColunaTotalKg))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if valor== "TotalKg":
			valor=0 
		if valor== "":
			valor=0
		valor=float(valor)
		if viaturas[i]["viatura"]==nomeViatura and codestadoguia!=estadocancelado:
			viaturas[i]["totalkg"]=viaturas[i]["totalkg"]+ valor

#-------------------------------------------
#---------UPDATE NUMERO DE DATAS------------
#-------------------------------------------

# percorrer xls a procura de Datas em que cada carro trabalha
for n in range(len(viaturas)):
	ArrayDatas=[]
	for i in range(sh.nrows):
		nomeViatura=format(sh.cell_value(rowx=i, colx=ColunaViatura))
		Data=format(sh.cell_value(rowx=i,colx=ColunaDataGuia))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		v1=Data
		if viaturas[n]["viatura"]==nomeViatura and codestadoguia!=estadocancelado:
			if v1 in ArrayDatas:
				k1=0
			else:
				ArrayDatas.append(Data)	
		viaturas[n]["numdias"]=len(ArrayDatas)

# percorrer xls a procura do numero total de Datas do documento
ArrayDatasTotal=[]
NumDiasTotal=0
for i in range(sh.nrows):
	Data=format(sh.cell_value(rowx=i,colx=ColunaDataGuia))
	v1=Data
	if v1 in ArrayDatasTotal:
		k1=0
	else:
		ArrayDatasTotal.append(Data)
	NumDiasTotal=len(ArrayDatasTotal)-1

# update terminal progress bar
pbar.update(35)

# ---------------------
# Update Clientes
# ------------------------

# array clientes
clientes=[]

#adicionar carros
for n in range(sh.nrows):
	nomeCliente=format(sh.cell_value(rowx=n, colx=ColunaCliente))
	v1={"cliente":nomeCliente,"entregas":0, "recolhas":0, "devolucoes":0, "prefatura":0, "faturado":0, "numdias":0, "pfpdt":0, "fpdt":0, "totalkg":0, "kgpdt":0, "valorkg":0, "pvalorkg":0, "minfd":0}
	if v1 in clientes:
		i=0
	else:
		clientes.append({"cliente":nomeCliente, "entregas":0, "recolhas":0, "devolucoes":0, "prefatura":0, "faturado":0, "numdias":0, "pfpdt":0, "fpdt":0, "totalkg":0,"kgpdt":0, "valorkg":0, "pvalorkg":0, "minfd":0})

# remover primeiro elemento pois e 'cliente'
clientes.pop(0)

# percorrer xls a procura de ENTREGAS feitas por cada Cliente
for n in range(sh.nrows):
	for i in range(len(clientes)):
		nomeCliente=format(sh.cell_value(rowx=n, colx=ColunaCliente))
		tipoGuia=format(sh.cell_value(rowx=n,colx=ColunaTipoGuia))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if clientes[i]["cliente"]==nomeCliente and tipoGuia=="DIST" and codestadoguia!=estadocancelado:
			clientes[i]["entregas"]=clientes[i]["entregas"]+1

# percorrer xls a procura de RECOLHAS feitas por cada Cliente
for n in range(sh.nrows):
	for i in range(len(clientes)):
		nomeCliente=format(sh.cell_value(rowx=n, colx=ColunaCliente))
		tipoGuia=format(sh.cell_value(rowx=n,colx=ColunaTipoGuia))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if clientes[i]["cliente"]==nomeCliente and tipoGuia=="REC" and codestadoguia!=estadocancelado:
			clientes[i]["recolhas"]=clientes[i]["recolhas"]+1

# percorrer xls a procura de DEVOLUCOES feitas por cada Cliente
for n in range(sh.nrows):
	for i in range(len(clientes)):
		nomeCliente=format(sh.cell_value(rowx=n, colx=ColunaCliente))
		tipoGuia=format(sh.cell_value(rowx=n,colx=ColunaTipoGuia))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if clientes[i]["cliente"]==nomeCliente and tipoGuia=="DEV" and codestadoguia!=estadocancelado:
			clientes[i]["devolucoes"]=clientes[i]["devolucoes"]+1

# percorrer xls a procura do valor facturado por cada Cliente
for n in range(sh.nrows):
	for i in range(len(clientes)):
		nomeCliente=format(sh.cell_value(rowx=n, colx=ColunaCliente))
		valor=format(sh.cell_value(rowx=n,colx=ColunaValorFatura))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if valor== "ValorFatura":
			valor=0 
		if valor== "":
			valor=0
		valor=float(valor)
		if clientes[i]["cliente"]==nomeCliente and codestadoguia!=estadocancelado:
			clientes[i]["faturado"]=clientes[i]["faturado"]+ valor

# percorrer xls a procura do valor Previsto de faturacao por cada Cliente
for n in range(sh.nrows):
	for i in range(len(clientes)):
		nomeCliente=format(sh.cell_value(rowx=n, colx=ColunaCliente))
		valor=format(sh.cell_value(rowx=n,colx=ColunaCalculoValor))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if valor== "CalculoValor":
			valor=0 
		if valor== "":
			valor=0
		valor=float(valor)
		if clientes[i]["cliente"]==nomeCliente and codestadoguia!=estadocancelado:
			clientes[i]["prefatura"]=clientes[i]["prefatura"]+ valor

# percorrer xls a procura do valor de KG de cada carga por cada Cliente
for n in range(sh.nrows):
	for i in range(len(clientes)):
		nomeCliente=format(sh.cell_value(rowx=n, colx=ColunaCliente))
		valor=format(sh.cell_value(rowx=n,colx=ColunaTotalKg))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		if valor== "TotalKg":
			valor=0 
		if valor== "":
			valor=0
		valor=float(valor)
		if clientes[i]["cliente"]==nomeCliente and codestadoguia!=estadocancelado:
			clientes[i]["totalkg"]=clientes[i]["totalkg"]+ valor

# percorrer xls a procura de Datas em que se efetuam servicos para o Cliente
for n in range(len(clientes)):
	ArrayDatas=[]
	for i in range(sh.nrows):
		nomeCliente=format(sh.cell_value(rowx=i, colx=ColunaCliente))
		Data=format(sh.cell_value(rowx=i,colx=ColunaDataGuia))
		codestadoguia=format(sh.cell_value(rowx=n,colx=ColunaCodEstadoGuia))
		v1={"data":Data}
		if clientes[n]["cliente"]==nomeCliente and codestadoguia!=estadocancelado:
			if v1 in ArrayDatas:
				k1=0
			else:
				ArrayDatas.append({"data":Data})	
		clientes[n]["numdias"]=len(ArrayDatas)

# update terminal progress bar
pbar.update(20)
# ----------------------
# CALCULOS POR MATRICULA
# ----------------------

# calculo da media de faturacao por dia de trabalho
for n in range(len(viaturas)):
	viaturas[n]["fpdt"]=float(viaturas[n]["faturado"]/viaturas[n]["numdias"])

# calculo da media de Faturacao Prevista por dia de trabalho
for n in range(len(viaturas)):
	viaturas[n]["pfpdt"]=float(viaturas[n]["prefatura"]/viaturas[n]["numdias"])

# calculo da media de KiloGramas por dia de trabalho
for n in range(len(viaturas)):
	viaturas[n]["kgpdt"]=float(viaturas[n]["totalkg"]/viaturas[n]["numdias"])

# calculo do valor por 1 kg 
for n in range(len(viaturas)):
	viaturas[n]["valorkg"]=float(viaturas[n]["faturado"]/viaturas[n]["totalkg"])

# calculo do valor PREVISTO por 1 kg 
for n in range(len(viaturas)):
	viaturas[n]["pvalorkg"]=float(viaturas[n]["prefatura"]/viaturas[n]["totalkg"])

# ----------------------
# CALCULOS POR CLIENTE
# ----------------------

# update na tabela de precos ->remocao do shuttel(300 euros)
# # Lusocargo Porto
# # subtarir valor incluido pelo shuttles
# for i in range(len(clientes)):
# 	string="LUSOCARGO -TRANSITÁRIOS S.A"
# 	# string=string.decode("utf8")
# 	if clientes[i]["cliente"]==string:
# 		# valor a subtrair=dias de trabalho*300 euros
# 		valorSubtrair=clientes[i]["numdias"]*300
# 		clientes[i]["faturado"]=clientes[i]["faturado"]-valorSubtrair
# 		clientes[i]["prefatura"]=clientes[i]["prefatura"]-valorSubtrair

# calculo da media de faturacao por dia de trabalho
for n in range(len(clientes)):
	clientes[n]["fpdt"]=float(clientes[n]["faturado"]/clientes[n]["numdias"])

# calculo da media de Faturacao Prevista por dia de trabalho
for n in range(len(clientes)):
	clientes[n]["pfpdt"]=float(clientes[n]["prefatura"]/clientes[n]["numdias"])

# calculo da media de KiloGramas por dia de trabalho
# for n in range(len(clientes)):
	# clientes[n]["kgpdt"]=float(clientes[n]["totalkg"]/clientes[n]["numdias"])

# calculo do valor por 1 kg 
for n in range(len(clientes)):
	clientes[n]["valorkg"]=float(clientes[n]["faturado"]/clientes[n]["totalkg"])

# calculo do valor PREVISTO por 1 kg 
for n in range(len(clientes)):
	clientes[n]["pvalorkg"]=float(clientes[n]["prefatura"]/clientes[n]["totalkg"])

# update terminal progress bar
pbar.update(20)
#-----------------------------
# UPDATE 
# ordenar viaturas media de faturacao prevista por dia de trabalho
# ----------------------------------

viaturas = sorted(viaturas, key=itemgetter("pfpdt"),reverse=True)
# for n in range(len(viaturas)):
# 	print(viaturas[n]["viatura"],viaturas[n]["pfpdt"])

# ----------------------------
# Histograma
# ----------------------------

# Array nome viaturas
viaturasGraph=[]
pfpdtGraph=[]
minGraph=[]
for n in range(len(viaturas)):
	viaturasGraph.append(viaturas[n]["viatura"])
	pfpdtGraph.append(viaturas[n]["pfpdt"])
	minGraph.append(viaturas[n]["minfd"])

# resize figure
plt.figure(figsize=(10, 10))

for n in range(len(pfpdtGraph)):
	# print(pfpdtGraph[n])
	if viaturasGraph[n]=="ARMAZEM":
		plt.barh(viaturasGraph[n], pfpdtGraph[n]*100/500, color='m')
	elif minGraph[n]==0:
		plt.barh(viaturasGraph[n], pfpdtGraph[n]*100/500, color='b')
	elif pfpdtGraph[n]>minGraph[n]:
		plt.barh(viaturasGraph[n], pfpdtGraph[n]*100/500, color='g')
	elif pfpdtGraph[n] >= minGraph[n]-50 and pfpdtGraph[n]<minGraph[n]:
		plt.barh(viaturasGraph[n], pfpdtGraph[n]*100/500, color='y')
	else:
		plt.barh(viaturasGraph[n], pfpdtGraph[n]*100/500, color='r')
# Nota: pdpftGraph*100/500 para fazer a percentagem do valor obtido sendo o máximo 500€

plt.ylabel("Viaturas")
plt.xlabel("Percentagem %")

# plt.show()

# guardar imagem como bmp
plt.savefig('plot.png')

# abrir imagens para fazer resize para 700*700
img = Image.open("plot.png")
img = resizeimage.resize('thumbnail', img, [700, 700])
img.save('plot2.png', img.format)

pbar.update(20)

# como o XLWT nao suporta imagens png 
# tem que se transformar a imagem png em bmp
file_in = "plot2.png"
img = Image.open(file_in)
file_out = 'plot.bmp'
# print (len(img.split()))  # test
if len(img.split()) == 4:
    # prevent IOError: cannot write mode RGBA as BMP
    r, g, b, a = img.split()
    img = Image.merge("RGB", (r, g, b))
    img.save(file_out)
else:
    img.save(file_out)



# --------------------------
# Update titulo e datas de analise no topo
# ------------------------

# print(ArrayDatasTotal[0])

# Eliminar primeiro elemento
ArrayDatasTotal.pop(0)
# ordenar
ArrayDatasTotal.sort()
# armazenar variaveis
DataInicio=ArrayDatasTotal[0]
DataFim=ArrayDatasTotal[len(ArrayDatasTotal)-1]


# -------------------------------
# UPDATE estilos valores previstos cores
# -------------------------------

#estilos
Ctitulo = xlwt.easyxf("font: name Times New Roman, color-index black, bold on")
Caviso = xlwt.easyxf("font: name Times New Roman, color-index red, bold on")
Cazul = xlwt.easyxf("pattern:pattern solid, fore-colour light_blue; font:color-index black, bold on")
Cverde = xlwt.easyxf("pattern:pattern solid, fore-colour green; font:color-index black, bold on")
Camarelo = xlwt.easyxf("pattern:pattern solid, fore-colour yellow; font:color-index black, bold on")
Cvermelho = xlwt.easyxf("pattern:pattern solid, fore-colour red; font:color-index black, bold on")
Croxo = xlwt.easyxf("pattern:pattern solid, fore-colour purple_ega; font:color-index black, bold on")
# escrever num novo ficheiro xls resultados
wb = xlwt.Workbook()
ws = wb.add_sheet('Analise Viaturas')

# ----------------
# Update titulos
# ----------------

# titulos
ws.write(0, 4, 'TFD Analise Viaturas',Ctitulo)
ws.write(0, 6, DataInicio,Ctitulo)
ws.write(0, 7, "a",Ctitulo)
ws.write(0, 8, DataFim,Ctitulo)

# subtitulos
ws.write(2, 0, 'VIATURA',Ctitulo)
ws.write(2, 1, 'ENTREGAS')
ws.write(2, 2, 'RECOLHAS')
ws.write(2, 3, 'DEVOLUCOES')
ws.write(2, 4, 'DIAS TRABALHO')
ws.write(2, 5, 'PREV FATURADO')
ws.write(2, 6, 'MEDIA PF/DT')
ws.write(2, 7, 'FATURADO')
ws.write(2, 8, 'MEDIA F/DT')
ws.write(2, 9, 'TotalKg')
ws.write(2, 10, 'Kg/DT')
ws.write(2, 11, 'Previsto Kg/euro')
ws.write(2, 12, 'Kg/euro')



# minimos valores preco kg 
bomkg=0.05
maisoumenoskg=0.045

# escrever dados correspondente a cada viatura
for i in range(len(viaturas)):
	ws.write(i+3,0,viaturas[i]["viatura"])
	ws.write(i+3,1,viaturas[i]["entregas"])
	ws.write(i+3,2,viaturas[i]["recolhas"])
	ws.write(i+3,3,viaturas[i]["devolucoes"])
	ws.write(i+3,4,viaturas[i]["numdias"])
	ws.write(i+3,5,viaturas[i]["prefatura"])
	if viaturas[i]["viatura"]=="ARMAZEM":
		ws.write(i+3,6,viaturas[i]["pfpdt"],Croxo)
	elif viaturas[i]["minfd"]==0:
		ws.write(i+3,6,viaturas[i]["pfpdt"],Cazul)
	elif viaturas[i]["pfpdt"]>=viaturas[i]["minfd"]:
		ws.write(i+3,6,viaturas[i]["pfpdt"],Cverde)
	elif viaturas[i]["pfpdt"]>=viaturas[i]["minfd"]-50 and viaturas[i]["pfpdt"]<viaturas[i]["minfd"]:
		ws.write(i+3,6,viaturas[i]["pfpdt"],Camarelo)	
	else:
		ws.write(i+3,6,viaturas[i]["pfpdt"],Cvermelho)
	if viaturas[i]["faturado"]!=viaturas[i]["prefatura"]:
		ws.write(i+3,7,viaturas[i]["faturado"],Caviso)
	else:
		ws.write(i+3,7,viaturas[i]["faturado"])
	ws.write(i+3,8,viaturas[i]["fpdt"])
	ws.write(i+3,9,viaturas[i]["totalkg"])
	ws.write(i+3,10,viaturas[i]["kgpdt"])
	if viaturas[i]["viatura"]=="ARMAZEM":
		ws.write(i+3,11,viaturas[i]["pvalorkg"],Croxo)
	elif viaturas[i]["pvalorkg"]>=bomkg:
		ws.write(i+3,11,viaturas[i]["pvalorkg"],Cverde)
	elif viaturas[i]["pvalorkg"]>=maisoumenoskg and viaturas[i]["pvalorkg"]<bomkg:
		ws.write(i+3,11,viaturas[i]["pvalorkg"],Camarelo)
	else:
		ws.write(i+3,11,viaturas[i]["pvalorkg"],Cvermelho) 

	ws.write(i+3,12,viaturas[i]["valorkg"])


#Valores Totais Viaturas
TotalEntregas=0
TotalRecolhas=0
TotalDevolucoes=0
TotalPreFatura=0
TotalFaturado=0
TotalKilograma=0
for n in range(len(viaturas)):
	TotalEntregas=TotalEntregas+viaturas[n]["entregas"]
	TotalRecolhas=TotalRecolhas+viaturas[n]["recolhas"]
	TotalDevolucoes=TotalDevolucoes+viaturas[n]["devolucoes"]
	TotalPreFatura=TotalPreFatura+viaturas[n]["prefatura"]
	TotalFaturado=TotalFaturado+viaturas[n]["faturado"]
	TotalKilograma=TotalKilograma+viaturas[n]["totalkg"]

# escrever no final do ficheiro os TOTAIS
linhatotal=len(viaturas)+4
ws.write(linhatotal,0, "TOTAL",Ctitulo)
ws.write(linhatotal,1, TotalEntregas)
ws.write(linhatotal,2, TotalRecolhas)
ws.write(linhatotal,3, TotalDevolucoes)
ws.write(linhatotal,4, NumDiasTotal)
ws.write(linhatotal,5, TotalPreFatura)
ws.write(linhatotal,6, TotalPreFatura/NumDiasTotal)
ws.write(linhatotal,7, TotalFaturado)
ws.write(linhatotal,8, TotalFaturado/NumDiasTotal)
ws.write(linhatotal,9, TotalKilograma)
ws.write(linhatotal,10, TotalKilograma/NumDiasTotal)
if TotalPreFatura/TotalKilograma>=bomkg:
	ws.write(linhatotal,11, TotalPreFatura/TotalKilograma,Cverde)
elif TotalPreFatura/TotalKilograma>=maisoumenoskg and TotalPreFatura/TotalKilograma<bomkg:
	ws.write(linhatotal,11, TotalPreFatura/TotalKilograma,Camarelo)
else:
	ws.write(linhatotal,11, TotalPreFatura/TotalKilograma,Cvermelho)
# ws.write(linhatotal,11, TotalPreFatura/TotalKilograma)
ws.write(linhatotal,12, TotalFaturado/TotalKilograma)

# escrever o numero total de servicos
ws.write(linhatotal+2,1, "Servicos",Ctitulo)
ws.write(linhatotal+2,2, TotalEntregas + TotalRecolhas,Ctitulo)

# escrever a diferenca entre o previsto faturar e o que ja foi faturado
ws.write(linhatotal+2,4, "Falta Faturar",Caviso)
ws.write(linhatotal+2,5, TotalPreFatura - TotalFaturado,Caviso)

# descricao da legenda
ws.write(linhatotal+4,0, "Legenda:")
ws.write(linhatotal+4,1, "",Cverde)
ws.write(linhatotal+4,2, "Bom Resultado")
ws.write(linhatotal+5,1, "",Camarelo)
ws.write(linhatotal+5,2, "Resultado mediano")
ws.write(linhatotal+6,1, "",Cvermelho)
ws.write(linhatotal+6,2, "Mau Resultado")
ws.write(linhatotal+7,1, "",Cazul)
ws.write(linhatotal+7,2, "Preco Minimo nao existe na tabela")

# ---------------
# Folha CLIENTES
# ---------------
wc = wb.add_sheet('Analise Clientes')

# titulos
wc.write(0, 4, 'TFD Analise Clientes',Ctitulo)
wc.write(0, 6, DataInicio,Ctitulo)
wc.write(0, 7, "a",Ctitulo)
wc.write(0, 8, DataFim,Ctitulo)

# subtitulos
wc.write(2, 0, 'CLIENTES',Ctitulo)
wc.write(2, 1, 'ENTREGAS')
wc.write(2, 2, 'RECOLHAS')
wc.write(2, 3, 'DEVOLUCOES')
wc.write(2, 4, 'DIAS TRABALHO')
wc.write(2, 5, 'PREV FATURADO')
wc.write(2, 6, 'MEDIA PF/DT')
wc.write(2, 7, 'FATURADO')
wc.write(2, 8, 'MEDIA F/DT')
wc.write(2, 9, 'TotalKg')
wc.write(2, 10, 'Previsto Kg/euro')
wc.write(2, 11, 'Kg/euro')

# escrever dados correspondente a cada CLIENTE
for i in range(len(clientes)):
	wc.write(i+3,0,clientes[i]["cliente"])
	wc.write(i+3,1,clientes[i]["entregas"])
	wc.write(i+3,2,clientes[i]["recolhas"])
	wc.write(i+3,3,clientes[i]["devolucoes"])
	wc.write(i+3,4,clientes[i]["numdias"])
	wc.write(i+3,5,clientes[i]["prefatura"])
	wc.write(i+3,6,clientes[i]["pfpdt"])
	if clientes[i]["faturado"]!=clientes[i]["prefatura"]:
		wc.write(i+3,7,clientes[i]["faturado"],Caviso)
	else:
		wc.write(i+3,7,clientes[i]["faturado"])
	wc.write(i+3,8,clientes[i]["fpdt"])
	wc.write(i+3,9,clientes[i]["totalkg"])
	if clientes[i]["cliente"]=="ARMAZEM":
		wc.write(i+3,10,clientes[i]["pvalorkg"],Croxo)
	elif clientes[i]["pvalorkg"]>=bomkg:
		wc.write(i+3,10,clientes[i]["pvalorkg"],Cverde)
	elif clientes[i]["pvalorkg"]>=maisoumenoskg and clientes[i]["pvalorkg"]<bomkg:
		wc.write(i+3,10,clientes[i]["pvalorkg"],Camarelo)
	else:
		wc.write(i+3,10,clientes[i]["pvalorkg"],Cvermelho) 

	wc.write(i+3,11,clientes[i]["valorkg"])

#Valores Totais Viaturas
TotalEntregas=0
TotalRecolhas=0
TotalDevolucoes=0
TotalPreFatura=0
TotalFaturado=0
TotalKilograma=0
for n in range(len(clientes)):
	TotalEntregas=TotalEntregas+clientes[n]["entregas"]
	TotalRecolhas=TotalRecolhas+clientes[n]["recolhas"]
	TotalDevolucoes=TotalDevolucoes+clientes[n]["devolucoes"]
	TotalPreFatura=TotalPreFatura+clientes[n]["prefatura"]
	TotalFaturado=TotalFaturado+clientes[n]["faturado"]
	TotalKilograma=TotalKilograma+clientes[n]["totalkg"]

# escrever no final do ficheiro os TOTAIS
linhatotal=len(clientes)+4
wc.write(linhatotal,0, "TOTAL",Ctitulo)
wc.write(linhatotal,1, TotalEntregas)
wc.write(linhatotal,2, TotalRecolhas)
wc.write(linhatotal,3, TotalDevolucoes)
wc.write(linhatotal,4, NumDiasTotal)
wc.write(linhatotal,5, TotalPreFatura)
wc.write(linhatotal,6, TotalPreFatura/NumDiasTotal)
wc.write(linhatotal,7, TotalFaturado)
wc.write(linhatotal,8, TotalFaturado/NumDiasTotal)
wc.write(linhatotal,9, TotalKilograma)
# wc.write(linhatotal,10, TotalKilograma/NumDiasTotal)
if TotalPreFatura/TotalKilograma>=bomkg:
	wc.write(linhatotal,10, TotalPreFatura/TotalKilograma,Cverde)
elif TotalPreFatura/TotalKilograma>=maisoumenoskg and TotalPreFatura/TotalKilograma<bomkg:
	wc.write(linhatotal,10, TotalPreFatura/TotalKilograma,Camarelo)
else:
	wc.write(linhatotal,10, TotalPreFatura/TotalKilograma,Cvermelho)
# wc.write(linhatotal,11, TotalPreFatura/TotalKilograma)
wc.write(linhatotal,11, TotalFaturado/TotalKilograma)


# escrever o numero total de servicos
wc.write(linhatotal+2,1, "Servicos",Ctitulo)
wc.write(linhatotal+2,2, TotalEntregas + TotalRecolhas,Ctitulo)

# escrever a diferenca entre o previsto faturar e o que ja foi faturado
wc.write(linhatotal+2,4, "Falta Faturar",Caviso)
wc.write(linhatotal+2,5, TotalPreFatura - TotalFaturado,Caviso)

# descricao da legenda
wc.write(linhatotal+4,0, "Legenda:")
wc.write(linhatotal+4,1, "",Cverde)
wc.write(linhatotal+4,2, "Bom Resultado")
wc.write(linhatotal+5,1, "",Camarelo)
wc.write(linhatotal+5,2, "Resultado mediano")
wc.write(linhatotal+6,1, "",Cvermelho)
wc.write(linhatotal+6,2, "Mau Resultado")
wc.write(linhatotal+7,1, "",Cazul)
wc.write(linhatotal+7,2, "Preco Minimo nao existe na tabela")

# Notas
wc.write(linhatotal+4,5, "Nota:",Ctitulo)
wc.write(linhatotal+4,6, "O resultado apresentado pela Lusocago Porto nao contem o valor dos shuttles")

# --------------------------
# Folha Genérica
# --------------------------

wd = wb.add_sheet('Analise Distrubuicao')

# titulos
wd.write(0, 4, 'TFD Analise Distribuicao', Ctitulo)
wd.write(0, 6, DataInicio, Ctitulo)
wd.write(0, 7, "a", Ctitulo)
wd.write(0, 8, DataFim, Ctitulo)

# subtitulos
wd.write(2, 0, 'VIATURA', Ctitulo)
wd.write(2, 1, 'ENTREGAS')
wd.write(2, 2, 'RECOLHAS')
wd.write(2, 3, 'DEVOLUCOES')
wd.write(2, 4, 'DIAS TRABALHO')
wd.write(2, 5, 'MEDIA PF/DT')

# minimos valores preco kg
bomkg = 0.05
maisoumenoskg = 0.045

# escrever dados correspondente a cada viatura
for i in range(len(viaturas)):
	wd.write(i+3, 0, viaturas[i]["viatura"])
	wd.write(i+3, 1, viaturas[i]["entregas"])
	wd.write(i+3, 2, viaturas[i]["recolhas"])
	wd.write(i+3, 3, viaturas[i]["devolucoes"])
	wd.write(i+3, 4, viaturas[i]["numdias"])
	if viaturas[i]["viatura"] == "ARMAZEM":
		wd.write(i+3, 5, "",Croxo)
	elif viaturas[i]["minfd"] == 0:
		wd.write(i+3, 5, "", Cazul)
	elif viaturas[i]["pfpdt"] >= viaturas[i]["minfd"]:
		wd.write(i+3, 5, "", Cverde)
	elif viaturas[i]["pfpdt"] >= viaturas[i]["minfd"]-50 and viaturas[i]["pfpdt"] < viaturas[i]["minfd"]:
		wd.write(i+3, 5, "", Camarelo)
	else:
		wd.write(i+3, 5, "", Cvermelho)

#Valores Totais Viaturas
TotalEntregas = 0
TotalRecolhas = 0
TotalDevolucoes = 0
TotalPreFatura = 0
TotalFaturado = 0
TotalKilograma = 0
for n in range(len(viaturas)):
	TotalEntregas = TotalEntregas+viaturas[n]["entregas"]
	TotalRecolhas = TotalRecolhas+viaturas[n]["recolhas"]
	TotalDevolucoes = TotalDevolucoes+viaturas[n]["devolucoes"]
	TotalPreFatura = TotalPreFatura+viaturas[n]["prefatura"]
	TotalFaturado = TotalFaturado+viaturas[n]["faturado"]
	TotalKilograma = TotalKilograma+viaturas[n]["totalkg"]

# escrever no final do ficheiro os TOTAIS
linhatotal = len(viaturas)+4
wd.write(linhatotal, 0, "TOTAL", Ctitulo)
wd.write(linhatotal, 1, TotalEntregas)
wd.write(linhatotal, 2, TotalRecolhas)
wd.write(linhatotal, 3, TotalDevolucoes)
wd.write(linhatotal, 4, NumDiasTotal)

# descricao da legenda
wd.write(linhatotal+4, 0, "Legenda:")
wd.write(linhatotal+4, 1, "", Cverde)
wd.write(linhatotal+4, 2, "Bom Resultado")
wd.write(linhatotal+5, 1, "", Camarelo)
wd.write(linhatotal+5, 2, "Resultado mediano")
wd.write(linhatotal+6, 1, "", Cvermelho)
wd.write(linhatotal+6, 2, "Mau Resultado")
wd.write(linhatotal+7, 1, "", Cazul)
wd.write(linhatotal+7, 2, "Preco Minimo nao existe na tabela")

# adicionar plot de viaturas/previsto faturado por dia de trabalho
wd.insert_bitmap(file_out, 3, 7)

# eliminar imagens criadas
os.remove("plot.png")
os.remove("plot2.png")
os.remove("plot.bmp")

# -----------------------------
# Save final do Ficheiro criado
# -----------------------------

# guardar com nome do ficheiro original + analise
last=filename.split('/')[-1]
final=last.split('.')[0]
final=final+"analise.xls"
wb.save(final)

# mensagem de SUCESSO
messagebox.showinfo("SUCESSO!!","Ficheiro {0} criado".format(final))
